function loadData(){
    let request = sendRequest('/listar', 'GET', '');
    let table = document.getElementById('products-table');
    table.innerHTML = "";
    request.onload = function(){
        let data = request.response;
        data.forEach((element, index) => {
            table.innerHTML += `
                <tr>
                    <td>${element._id}</td>
                    <td>${element.nombre}</td>
                    <td>${element.precio}</td>
                    <td>${element.descripcion}</td>
                    <td>${element.imagen}</td>
                    <td>${element.stock}</td>
                    <td>
                        <button type="button" class="btn btn-primary" 
                        onclick='window.location = "form_productos.html?id=${element._id}"'>
                        Editar                            
                        </button>
                        <button type="button" class="btn btn-warning" id="borrar" onclick="deleteProducto('${element._id}')">Borrar</button>
                    </td>
                </tr>
                `
        });
    }
    request.onerror = function(){
        table.innerHTML = `
            <tr>
                <td colspan="6">Error al recuperar los datos.</td>
            </tr>
            `;
    }
}

//Guarda el producto
function saveProducto(){
    let nombre = document.getElementById('product-name').value;
    let precio = document.getElementById('product-precio').value;
    let descripcion = document.getElementById('product-desc').value;
    let imagen = document.getElementById('product-img').value;
    let stock = document.getElementById('product-stock').value;
    let data = {'nombre': nombre, 'precio': precio, 'descripcion': descripcion, 'imagen': imagen, 'stock': stock }
    let request = sendRequest('/crear','POST', data);
    request.onload = function(){
        window.location = 'adminProduct.html';
    }
    request.onerror = function(){
        alert('Error al guardar los cambios')
    }
}

//consulta el producto por el id
function loadProducto(id){
    let request = sendRequest('/buscar/'+id,'GET','');
    let nombre = document.getElementById('product-name');
    let precio = document.getElementById('product-precio');
    let descripcion = document.getElementById('product-desc');
    let imagen = document.getElementById('product-img');
    let stock = document.getElementById('product-stock');
    id = document.getElementById('product-id');

    request.onload = function(){
        let data = request.response.result   
        nombre.value = data.nombre
        precio.value = data.precio
        descripcion.value = data.descripcion
        imagen.value = data.imagen
        stock.value = data.stock    
        id.value = data._id    
    }
    request.onerror = function(){
        alert("Error al recuperar los datos");
    }
}

//eliminar el producto
function deleteProducto(id){
    //alert(id);
    let data = {'_id': id}
    let request = sendRequest('/eliminarProducto/'+ id,'DELETE',data);
    console.log(request);
    request.onload = function(){
        window.location = 'adminProduct.html';
    }
    request.onerror = function(){
        alert('Error al eliminar el producto')
    }
}

//modificar el producto
function updateProductos(){
    let nombre = document.getElementById('product-name').value;
    let precio = document.getElementById('product-precio').value;
    let descripcion = document.getElementById('product-desc').value;
    let imagen = document.getElementById('product-img').value;
    let stock = document.getElementById('product-stock').value;
    let id = document.getElementById('product-id').value;
    
    let data = {'nombre': nombre, 'precio': precio, 'descripcion': descripcion, 'imagen': imagen, 'stock': stock }
    let request = sendRequest('/modificarProducto/'+ id , 'PUT', data)
    request.onload = function(){
        window.location = 'adminProduct.html';
    }
    request.onerror = function(){
        alert('Error al editar los cambios')
    }
}
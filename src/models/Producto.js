const mongoose = require('mongoose');
    const ProductoSchema = mongoose.Schema({
        nombre: {
            type: String,
            required: true
        },
        precio: {
            type: Number,
            required: true
        },
        descripcion: {
            type: String,
            required: true
        },
        imagen: {
            type: String,
            required: true
        },
        stock: {
            type: Number,
            required: true
        }
    })

    const producto = mongoose.model('productos',ProductoSchema);
module.exports = producto;

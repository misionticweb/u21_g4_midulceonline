const producto = require("../models/Producto");
const user  = require("../models/Usuario");

function prueba(req, res) {
  res.status(200).send({
    message: "probando una acción",
  });
}

//funcion para guardar los productos
function saveProducto(req, res) {
  var productos = new producto(req.body);
    productos.save((err, result) => {
    res.status(200).send({ message: result });
  });
}

//funcion para buscar los productos por el id
function buscarData(req, res) {
  var idproducto = req.params.id;
  producto.findById(idproducto).exec((err, result) => {
    if (err) {
      res
        .status(500)
        .send({ message: "Error al momento de ejecutar la solicitud" });
    } else {
      if (!result) {
        res
          .status(404)
          .send({ message: "El registro a buscar no se encuentra disponible" });
      } else {
        res.status(200).send({ result });
      }
    }
  });
}

//funcion para listar todos los productos
function listarAllData(req, res) {
  var idproducto = req.params.idb;
  if (!idproducto) {
    var result = producto.find({}).sort("nombre");
  } else {
    var result = producto.find({ _id: idproducto }).sort("nombre");
  }
  result.exec(function (err, result) {
    if (err) {
      res
        .status(500)
        .send({ message: "Error al momento de ejecutar la solicitud" });
    } else {
      if (!result) {
        res
          .status(404)
          .send({ message: "El registro a buscar no se encuentra disponible" });
      } else {
        res.status(200).send( result );
      }
    }
  });
}

//funcion para actualizar los productos
function updateProductos(req, res) {
    var id = req.params.id;
    var data =req.body;
    producto.findByIdAndUpdate(id, data, { new: true }, (err, result) => {
    if (err){
      res.status(500).send( {message:err} );
    }else{
      res.status(200).send( result );   
     }
    });
};


//funcion para eliminar los productos
function deleteProductos(req, res) {
    var idproducto = req.params.id;
    
    producto.findByIdAndDelete(idproducto, (err, result) => {
    if(err) {
      res.status(500).send( {message: err });
    }else {
      res.status(200).send( result );
    }
    
    });
};


module.exports={
    prueba,
    saveProducto,
    buscarData,
    listarAllData,
    updateProductos,
    deleteProductos
};

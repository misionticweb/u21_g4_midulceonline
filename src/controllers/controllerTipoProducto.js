const tipoProducto = require("../models/tipoProducto");

function prueba(req, res) {
  res.status(200).send({
    message: "probando una acción",
  });
}


function saveTipoProducto(req, res) {
  var productos = new tipoProducto(req.body);
  productos.save((err, result) => {
    res.status(200).send({ message: result });
  });
}

function buscarData(req, res) {
  var idproducto = req.params.id;
  tipoProducto.findById(idproducto).exec((err, result) => {
    if (err) {
      res
        .status(500)
        .send({ message: "Error al momento de ejecutar la solicitud" });
    } else {
      if (!result) {
        res
          .status(404)
          .send({ message: "El registro a buscar no se encuentra disponible" });
      } else {
        res.status(200).send({ result });
      }
    }
  });
}
function listarAllData(req, res) {
  var idproducto = req.params.idb;
  if (!idproducto) {
    var result = tipoProducto.find({}).sort("nombre");
  } else {
    var result = tipoProducto.find({ _id: idproducto }).sort("nombre");
  }
  result.exec(function (err, result) {
    if (err) {
      res
        .status(500)
        .send({ message: "Error al momento de ejecutar la solicitud" });
    } else {
      if (!result) {
        res
          .status(404)
          .send({ message: "El registro a buscar no se encuentra disponible" });
      } else {
        res.status(200).send({ result });
      }
    }
  });
}
function updateTipoProducto(req,res){
    var id=req.params.id;
    var ObjectId = mongoose.Types.ObjectId(id);
    var data =req.body;
    tipoProducto.findByIdAndUpdate({_id: ObjectId}, data, {new: true}, function(err,tipoProducto) {
    if (err){
    res.status(500).send({message:err});
    }else{
      res.status(200).send(tipoProducto);   
     }
    });
};
function deleteTipoProducto(req,res){
    var idproducto=req.params.id;
    tipoProducto.findByIdAndRemove(idproducto, function(err, producto){
    if(err) {
    return res.json(500, {
    message: 'No hemos encontrado el producto'
    })
    }
    return res.json(producto)
    });
};
module.exports={
    prueba,
    saveTipoProducto,
    buscarData,
    listarAllData,
    updateTipoProducto,
    deleteTipoProducto
};

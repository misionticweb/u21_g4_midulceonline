const Usuario = require("../models/Usuario");
const bcryptjs = require("bcryptjs");
const { validationResult } = require("express-validator");
const jwt = require("jsonwebtoken");

async function guardarUser(req, res) {

 


  var usuario = new Usuario(req.body);
  usuario.save((err, result) => {
    res.status(200).send({ message: result });
  });
  }

function obtenerUsuario(req, res) {
  var idusuario = req.params.idb;
  if (!idusuario) {
    var result = Usuario.find({}).sort("nombre");
  } else {
    var result = Usuario.find({ _id: idusuario }).sort("nombre");
  }
  result.exec(function (err, result) {
    if (err) {
      res
        .status(500)
        .send({ message: "Error al momento de ejecutar la solicitud" });
    } else {
      if (!result) {
        res
          .status(404)
          .send({ message: "El registro a buscar no se encuentra disponible" });
      } else {
        res.status(200).send({ result });
      }    }
  });
};

module.exports={
 guardarUser,
 obtenerUsuario
};


//Rutas para crear usuarios

const express = require("express");
const router = express.Router();
var usuarioController = require("../controllers/usuariosController");
const { validationResult } = require("express-validator");
const jwt = require("jsonwebtoken");

//api/usuarios
router.get("/usuarios", usuarioController.obtenerUsuario);
router.post("/usuario", usuarioController.guardarUser);
module.exports = router;
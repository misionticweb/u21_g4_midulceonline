const { Router } = require('express');
var controller=require('../controllers/controllerProducto');
const router = Router();

router.get('/prueba', controller.prueba);
router.post('/crear',controller.saveProducto);
router.get('/buscar/:id',controller.buscarData);
router.get('/listar/',controller.listarAllData);
router.delete('/eliminarProducto/:id',controller.deleteProductos);
router.put('/modificarProducto/:id',controller.updateProductos);

module.exports=router;
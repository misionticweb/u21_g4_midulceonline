var express = require('express')
app = express();
bodyParser = require("body-parser");

mongoose = require('mongoose')
app.use(express.json());
app.use(express.urlencoded({
extended: true
}));

// Configurar cabeceras y cors
app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin','*');
    res.header('Access-Control-Allow-Credentials', true);
    res.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY, Origin, XRequested-With, Content-Type, Accept, Access-Control-Allow-Request-Method');
    res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
    res.header('Allow', 'GET, POST, OPTIONS, PUT, DELETE');
    next();
    });
  
    app.use(require('./src/routers/router'));
    //app.use(require('./src/routers/usuarios'));
    //app.use(require('./src/routers/auth'));
    app.use("/listar/", express.static("front-end"));
    app.use("/", express.static("front-end"));
    

    app.post("/api/pay", (req, res) => {
        const ids = req.body;
        const productsCopy = products.map(p => ({ ...p }));
        ids.forEach(id => {
          const product = productsCopy.find(p => p.id === id);
          if (product.stock > 0) {
            product.stock--;
          }
          else {
            throw ("sin stock");
          }
      
        });
        products = productsCopy;
        res.send(products);
      });
      

    module.exports = app;
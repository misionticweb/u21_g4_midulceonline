//const express = require('express')
//const bodyParser = require('body-parser')
var app = require('./app');
var mongoose =require('./src/conexDB/conn');
//const port = 4000;
const port = process.env.PORT || 5000;

/*app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

const products = [
  {
    id: 1,
    name: "Bubaloo",
    price: 50,
    image: "images/bubaloo.png",
    stock: 3,
  },
  {
    id: 2,
    name: "Caramelo Mix",
    price: 50,
    image: "images/caramelos_mix.png",
    stock: 3,
  },
  {
    id: 3,
    name: "Chocolate en crema Muu",
    price: 50,
    image: "images/crema_choco_muu.png",
    stock: 3,
  },
  {
    id: 4,
    name: "Dulce Fruticas Maracuya",
    price: 50,
    image: "images/fruticas_maracuya.png",
    stock: 50,
  },
  {
    id: 5,
    name: "Mashmellow",
    price: 50,
    image: "images/chocmelos.png",
    stock: 3,
  },
  {
    id: 6,
    name: "Galleta Festival",
    price: 50,
    image: "images/galletas-festival.png",
    stock: 3,
  },
];

//probando el listar productos
app.get('/api/listar', (req, res) => {
  res.send(products)
})

app.post("/api/pay", (req, res) => {
  const ids = req.body;
  const productsCopy = products.map(p => ({ ...p }));
  ids.forEach(id => {
    const product = productsCopy.find(p => p.id === id);
    if (product.stock > 0) {
      product.stock--;
    }
    else {
      throw ("sin stock");
    }

  });
  products = productsCopy;
  res.send(products);
});*/

app.listen(port, () => {
  console.log(`aplicacion escuchando por el puerto: ${port}`)
})